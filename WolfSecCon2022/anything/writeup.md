# ANYTHING

Were given the encrypted flag `wfa{oporteec_gvb_ogd}`.

Since this is all printable text and the curly braces are not encrypted we can deduce that this is probably some classical cipher.

Sice the flag format is `wsc{}` we can see that this is not doing the same operation on every letter (if it were then the `w` in the ciphertext would be something else).

The next obvious guess would be to try viginere, and using the hint `this could be encrypted with ANYTHING` we can try using that as our key. And we get the flag `wsc{vigenere_not_bad}`

Or we could just throw it into the boxentriq cipher analyzer and we would see that this is viginere and follow the same procedure ¯\\\_(ツ)\_/¯
