# EAV-Secure Diffie-Hellman

We are given `p = 320907854534300658334827579113595683489`, `g = 3` and `A = g^x \equiv 236498462734017891143727364481546318401 \pmod{p}`

Finding `x` is fairly easy. Sage kept dying on me so I just used the obviously superior PARI/GP

```gp
p = 320907854534300658334827579113595683489
g = Mod(3, p)
A = Mod(236498462734017891143727364481546318401, p)
dlog = znlog(A, g)
```

When trying to convert `x` into ASCII we just gets garbage. This implies that the flag is bigger than the modulus so we just need to add the modulus until we get something resembling the flag

Formally, we would say that the flag is in the residue class of `x` modulo `p`

```sage
from Crypto.Util.number import long_to_bytes

p = 320907854534300658334827579113595683489
g = Mod(3, p)
A = Mod(236498462734017891143727364481546318401, p)
dlog = 67514057458967447420279566091192598301
for x in range(1000000000):
	flag = long_to_bytes(p*x + dlog)
	if flag.startswith(b'wsc'):
		print(x, flag)
```

Eventually (at x = 8300951) we get `8300951 b'wsc{l0g_j4m_4tt4\xe2\x15\x14'`. The flag is missing the last part but we can guess it (log jam atta.. -> log jam attack). We get the flag `wsc{l0g_j4m_4tt4ck}`
