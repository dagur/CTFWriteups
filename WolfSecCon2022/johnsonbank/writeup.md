# Johnsons bank

This ones just a simple timing attack

From the note in the login screen we can see that the username is probably his name and we find that `williamjohnson` works.

From the provided source we can deduce that this is probably a timing attack
```
// The authentication code is written in JohnsonScript, a much, much slower version of JavaScript
```
```javascript
function checkPassword(userInput, correctPassword){
    return userInput === correctPassword;
}
```

Our team used some burp extension to do the timing attack
