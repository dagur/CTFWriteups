# unformunately sage uses python3 so this is python3 code :vomit:

p = 19489

with open('output.txt') as f:
	output = f.read()

def findIdx(ch):
	return [enum for enum, x in enumerate(output) if x == ch and enum != 0]

known = [('I', 2), ('S', 3), ('{', 4)]
S = set()
for char in known:
	possibilities = findIdx(char[0])
	s = []
	for poss in possibilities:
		roots = Mod(char[1], p).nth_root(poss - 1, all = True)
		s.extend(roots)
	if S:
		S &= set(s)
	else:
		S = set(s)

print('possible S: {}'.format(S))

for s in S:
	flag = 'AS'
	for x in range(2, 100):
		exponent = discrete_log(Mod(x, p), Mod(s, p))
		flag += output[exponent + 1]
	print(flag)
